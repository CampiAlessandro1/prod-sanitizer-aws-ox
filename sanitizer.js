const fs = require('fs');
let fileToSanitize = process.argv[2];

if (!fileToSanitize) {
    console.error("no file specified")
    return
}

let users;

try {
    users = JSON.parse(fs.readFileSync(fileToSanitize));
    console.log("file loaded");
} catch (err) {
    console.error('Error while parsing JSON data:', err);
}

users.forEach(user => {
    delete user.ip_address;
});

try {
    fs.writeFileSync('users.json', JSON.stringify(users, null, 2))
    console.log("file written");
} catch (error) {
    console.error("Error while writing file:\n", error);
}